import numpy as np
from operator import add
from operator import sub
import matplotlib.pyplot as plt

class Calc(object):
    def __init__(self,FEPpath,PSFpath,boxlength,lambda1,lambda2,lambdastep,args=0):
        self.FEP = FEPpath
        self.PSF = PSFpath
        self.length = boxlength
        self.l1 = lambda1
        self.l2 = lambda2
        self.dl = lambdastep
        self.FE = args
        
    def Run(self):
       self.direction = self.FEP.split('/')[-1].split('.')[0]
       self.atom = self.PSF.split('/')[-2][-1]
       self.energies = []
       
       file1 = open(self.FEP,'rt')
       for i, line in enumerate(file1):
           if line.split()[0] == "#Free":
               self.energies.append(float(line.split()[-1]))    
       file1.close() 
       
       self.charges = []
       self.j = 0     
       self.numatoms = 0 
       file2 = open(self.PSF)
       for i, line in enumerate(file2):
           if line.split() != []:
               if line.split()[-1] == '!NATOM':
                   self.numatoms = int(line.split()[0])
                   self.j = i+1
           if (i >= self.j and i <= self.j+self.numatoms-1 and i > 1):
               self.charges.append(float(line.split()[-3]))  
       file2.close()
       
       self.totalcharge = self.charges[int(self.atom)-1]
       
       self.corrections = []

       for i in np.arange(self.l1+self.dl,self.l2+self.dl,self.dl):
           self.corrections.append((1/2.0)*(-2.837/self.length)*((self.totalcharge*i)**2)*332.112)

       if self.direction == 'forward':
           self.FinalEnergy = map(add,self.energies,self.corrections)
       elif self.direction == 'backward':
           self.FinalEnergy = map(add,self.energies,[-1*x for x in self.corrections])
           self.FinalEnergy = [x-self.FinalEnergy[-1] for x in self.FinalEnergy]

    
           