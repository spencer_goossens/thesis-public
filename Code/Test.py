from CalcFEP import *

Forward = Calc("/Users/sgoossens/Repos/titration-md/jd1/forward.fepout","/Users/sgoossens/Repos/titration-md/jd1/asp_capped.psf",40,0,1,0.02)
Forward.Run()

Backward = Calc("/Users/sgoossens/Repos/titration-md/jd1/backward.fepout","/Users/sgoossens/Repos/titration-md/jd1/asp_capped.psf",40,0,1,0.02)
Backward.Run()

fig = plt.figure(figsize=(4,6))

ax1 = fig.add_subplot(2,1,1)

ax1.plot(np.arange(Forward.l1+Forward.dl,Forward.l2+Forward.dl,Forward.dl),Forward.FinalEnergy,'rs',markersize=6.0)
ax1.set_xlabel('Lambda')
ax1.set_ylabel('Free Energy')
ax1.set_title('Charging')

ax2 = fig.add_subplot(2,1,2)
          
ax2.plot(np.arange(Backward.l2-Backward.dl,Backward.l1-Backward.dl,-Backward.dl),Backward.FinalEnergy,'bo-')
ax2.set_xlabel('Lambda')
ax2.set_ylabel('Free Energy')
ax2.set_title('Discharging')

plt.tight_layout()

fig2 = plt.figure(figsize=(10,10))

ax3 = fig2.add_subplot(1,1,1)

ax3.plot(np.arange(Forward.l1+Forward.dl,Forward.l2+Forward.dl,Forward.dl),Forward.FinalEnergy,'rs',label="Charging")
ax3.set_xlabel('Lambda')
ax3.set_ylabel('Free Energy')

ax3.plot(np.arange(Backward.l2-Backward.dl,Backward.l1-Backward.dl,-Backward.dl),Backward.FinalEnergy,'bo-',label="Discharging")
ax3.set_xlabel('Lambda',fontsize='x-large')
ax3.set_ylabel('Free Energy',fontsize='x-large')
ax3.legend(loc=3,fontsize='x-large')
ax3.axis([0,1,-17,0])

plt.show()