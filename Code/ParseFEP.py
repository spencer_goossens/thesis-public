# Code to obtain the charging free energy for FEP simulations
import numpy as np
from operator import add
import matplotlib.pyplot as plt
path = "/Users/sgoossens/Repos/titration-md/jd1/backward.fepout"
psfpath = "/Users/sgoossens/Repos/titration-md/jd1/asp_capped.psf"
simtype = "only one"
direction = path.split('/')[-1].split('.')[0]
atom = psfpath.split('/')[-2][-1]
length = 40 #Ang
lambda1 = 0
lambda2 = 1
steps = .02

energies = []

file1 = open(path,'rt')

for i, line in enumerate(file1):
    if line.split()[0] == "#Free":
        energies.append(float(line.split()[-1]))

if direction == "backward":
    energies = [x+finaleng for x in energies]
    

file1.close()

charges = []
totalcharge = 0
j = 0     
numatoms = 0 
file2 = open(psfpath)
for i, line in enumerate(file2):
    if line.split() != []:
        if line.split()[-1] == '!NATOM':
            numatoms = int(line.split()[0])
            j = i+1
    if (i >= j and i <= j+numatoms-1 and i > 1):
        charges.append(float(line.split()[-3]))
        totalcharge += float(line.split()[-3])    
file2.close()

if simtype == "all but on":
    totalcharge = totalcharge - charges[int(float(atom)-1)]
elif simtype == "only one":
    totalcharge = charges[int(float(atom)-1)]

corrections = []

for i in np.arange(lambda1+steps,lambda2+steps,steps):
    corrections.append((1/2.0)*(-2.837/length)*((totalcharge*i)**2)*332.112)

FinalEnergy = map(add,energies,corrections)
if direction == "forward":
    finaleng = FinalEnergy[-1]

if direction == "forward":
    plt.plot(np.arange(lambda1+steps,lambda2+steps,steps),FinalEnergy,'ro-')
    plt.xlabel('Lambda')
    plt.ylabel('Free Energy')


path = "/Users/sgoossens/Repos/titration-md/jd1/backward.fepout"

elif direction == "backward":
    plt.plot(np.arange(lambda2,lambda1,-1*steps),[x*-1 for x in FinalEnergy],'bo-')
    plt.xlabel('Lambda')
    plt.ylabel('Free Energy')

plt.show()