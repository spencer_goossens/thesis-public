import pandas as pd
import csv

class Tables(object):
    
    def __init__(self,name,path,toggle):
        self.name = name
        self.path = path
        self.toggle = toggle
        

    def isfloat(self,x):
        try:
            a = float(x)
        except ValueError:
            return False
        else:
            return True
        
    def isint(self,x):
        try:
            a = int(x)
        except ValueError:
            return False
        else:
            return True
        
    def islist(self,x):
        if x[0] == '[':
            return True
        elif x[0] == '-':
            return True
        else:
            return False
            
    def isfun(self,x):
        if x[0] == '@':
            return True
        else:
            return False  
            
    def writeout(self):
        if self.toggle:
            self.data = pd.read_csv((self.name+'.csv'))
            #del data['alpha_f &']
            self.data.to_csv(("/Users/sgoossens/Repos/Thesis/" + self.name + ".txt"), sep = '\n', index = False)
    
    def addspace(self,x):
        self.newvect = []
        for val in x.split(' '):
            self.newvect.append(('~' + val + '~'))
        return ''.join(self.newvect)
            
    def infile(self):
        if self.toggle:
            csv_out = csv.writer(open((self.name+'.csv'),'w'), delimiter=',')
            file1 = open(self.path)
            for line in file1:
                row = []
                vals = line.strip().split(',')
                for val in vals:
                    if (self.isfloat(val.strip()) or self.isint(val.strip()) or self.islist(val.strip()) or self.isfun(val.strip())) and val != vals[-1]:
                        row.append(('$'+self.addspace(val.strip())+'$ &'))
                    elif (self.isfloat(val.strip()) or self.isint(val.strip()) or self.islist(val.strip()) or self.isfun(val.strip())) and val == vals[-1]:
                        row.append(('$'+self.addspace(val.strip())+'$'))
                    elif val != vals[-1]:
                        row.append((val.strip()+' &'))
                    else:
                        row.append((val.strip()))
                row.append('\\\ \hline ')
                csv_out.writerow(row)
            file1.close()